/* 
 * File:   Frame.h
 * Author: martin
 *
 * Created on 10. leden 2017, 15:33
 */

#ifndef FRAME_H
#define	FRAME_H
#include "JavaClass.h"

class Frame {
public:
    //Frame();
    Frame(JavaClass*, Method*);
    Frame(JavaClass* jclass, Method* method, int maxLocals, int maxStack);
    //Frame(const Frame& orig);
    Variable pop() {/*if (sp == 0) return NULL*/; sp--; return stack[sp];}
    void push(Variable v) {stack[sp] = v; sp++;}
    void setStack(int i, Variable v){stack[i] = v;}
    void incSp(int i) {sp+= i;}
    void incSp() {sp++;}
    void decSp(int i) {sp-= i;}
    void decSp() {sp--;}
    JavaClass * getClass(){return javaClass;}
    Method* getMethod() {return method;}
    void incPc() {pc++;}
    void incPc(int i) {pc+=i;}
    void decPc() {pc--;}
    void decPc(int i) {pc-=i;}
    int getPc(){return pc;}
    int getSp(){return sp;}
    Variable top(){/*if (sp == 0) return NULL*/; return stack[sp-1];}
    Variable peek(){/*if (sp == 0) return NULL*/; return stack[sp-2];}
    void setLocal(int i, Variable v) {locals[i] = v;}
    Variable getLocal(int i) {return locals[i];}
    Variable getStack(int i) {return stack[i];}
    int getMaxLocals() {return this->maxLocals;}
    bool parseCode(JavaClass *javaClass,Method * method);
    char * code;
    
    virtual ~Frame();
private:
    JavaClass* javaClass;
    Method * method;
    int sp;
    int pc;
    int maxStack;
    Variable * stack;
    int maxLocals;
    Variable * locals;
    int codeLen;        
};

#endif	/* FRAME_H */

