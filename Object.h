/* 
 * File:   Object.h
 * Author: martin
 *
 * Created on 12. leden 2017, 14:36
 */

#ifndef OBJECT_H
#define	OBJECT_H

#include "JavaClass.h"


class Object {
public:
    Object(){}
    Object(JavaClass * javaClass);
    Object(const Object& orig);
    virtual ~Object();
    
    JavaClass *getJavaClass() {return javaClass;};
    virtual Variable getField(int idx);
    virtual void setField(int idx, Variable var);
    bool isReachable(){        
       return reachable;}
    virtual void mark();
   
    bool isOld(){return (age>=10); }
    void unmark(){reachable = false;}
    void setReachable(bool b){reachable = b;}
    int getAge(){return age;}
    void incAge(){age++;}
    virtual void markChildren();
    virtual bool isArray(){return false;}
protected:
    JavaClass *javaClass;
    Variable* fields;
 
    bool reachable;
    int age;
   
};

#endif	/* OBJECT_H */

