/* 
 * File:   ClassHeap.cpp
 * Author: martin
 * 
 * Created on 12. leden 2017, 13:51
 */

#include <string.h>
#include <iostream>
#include "ClassHeap.h"
#include "Execution.h"
#include "constants.h"
using namespace std;

ClassHeap::ClassHeap() {
    this->heapSize = 100;
    this->heap = new JavaClass*[this->heapSize];
    this->classCnt = 0;
    this->lastClassName.length = 0;
    this->lastClassName.value = '\0';
    this->lastClass = NULL;
    
}

ClassHeap::ClassHeap(const ClassHeap& orig) {
}

ClassHeap::~ClassHeap() {
    for (int i = 0; i< this->classCnt; i++){
        for(int j=0; j< this->heap[i]->attributes_count; j++){
            free(this->heap[i]->attributes[j].info);
        }        
     
       
        for(int j=0; j< this->heap[i]->methods_count; j++){
            for (int k = 0 ; k< this->heap[i]->methods[j].attrs_count; k++){
                free(this->heap[i]->methods[j].attrs[k].info);
            }
            free(this->heap[i]->methods[j].attrs);
        }
     
        
        for(int j=0; j< this->heap[i]->const_pool_count-1; j++){
            if (this->heap[i]->items[j].tag == STRING_UTF8)
                  free(this->heap[i]->items[j].value.string.value);
        }  
     
       
        for(int j=0; j< this->heap[i]->fields_count; j++){
            for (int k = 0 ; k< this->heap[i]->fields[j].attrs_count; k++){
                free(this->heap[i]->fields[j].attrs[k].info);
            }
            free(this->heap[i]->fields[j].attrs);
        }
      free(this->heap[i]->attributes);
      free(this->heap[i]->items);
      free(this->heap[i]->methods);
      free(this->heap[i]->fields);
      
      free(this->heap[i]->interfaces);
      if (this->heap[i]->staticFieldsCnt > 0 ){
          delete[] this->heap[i]->staticFields;
          this->heap[i]->staticFields = NULL;
      }
      free(this->heap[i]);
      this->heap[i] = NULL;
    }
    delete[] this->heap;
    this->heap = NULL;
}

bool ClassHeap::addClass(JavaClass *javaClass) {
   
    javaClass->lastFieldName.length = 0;
    javaClass->lastMethodName.length = 0;
    javaClass->staticFieldsCnt = 0;
    if (this->classCnt >= this->heapSize) return false; // todo
   // this->heap[this->classCnt] = new JavaClass();
    this->heap[this->classCnt] = javaClass;
    this->classCnt++;
    
   
    return true;
}

void ClassHeap::startClinit(Execution * ex, JavaClass * javaClass){
    javaClass->staticFieldsCnt = 0;
     int methodIdx = getMethodIdxFromClass(javaClass, "<clinit>", "()V");
  // cout<<methodIdx<<endl;
    if (methodIdx == -1) return ;
   //cout<< "clinit";
   javaClass->staticFieldsCnt = javaClass->fields_count;
    javaClass->staticFields = new Variable[javaClass->fields_count];
    for (int i = 0; i< javaClass->staticFieldsCnt; i++){
        javaClass->staticFields[i].type = -1;
    }
    Frame *frame = new Frame(javaClass, &javaClass->methods[methodIdx]);
        
    ex->Execute(frame);   
    delete frame;
}

void ClassHeap::clinitAll(Execution * ex){
    for(int i = 0; i< classCnt; i++){
        startClinit(ex,heap[i]);
    }
}


JavaClass* ClassHeap::getClass(string className) {
    //inline cache
    JavaClass * cacheClass = checkCache(className);
    if (cacheClass != NULL) return cacheClass;
    for (int i = 0; i < this->classCnt; i++) {
        if (! strcmp(className.c_str(), this->heap[i]->file_name)) {
            this->lastClass = this->heap[i];
            this->lastClassName.value = (char*)className.c_str();
            this->lastClassName.length = className.size();
            return this->heap[i];
        }
    }
    return NULL;
}

JavaClass* ClassHeap::getClass(String className) {
    //inline cache
    JavaClass * cacheClass = checkCache(className);
    if (cacheClass != NULL) return cacheClass;
    
    for (int i = 0; i < this->classCnt; i++) {
     
        
        if (! compareString(className, get_item(this->heap[i],get_item(this->heap[i],this->heap[i]->this_class)->value.integer)->value.string)) {
            this->lastClass = this->heap[i];
            this->lastClassName = className;
            return this->heap[i];
        }
    }
    return NULL;
}

JavaClass* ClassHeap::getClass(int idx) {
    if (idx >= this->classCnt) return NULL;
    return this->heap[idx];
}
JavaClass * ClassHeap::checkCache(String className){
    if (! compareString(this->lastClassName, className)){
        return this->lastClass;
    }
    return NULL;
}
    
JavaClass * ClassHeap::checkCache(string className){
     if (! compareString(this->lastClassName, className)){
        return this->lastClass;
    }
    return NULL;
}