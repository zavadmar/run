/* 
 * File:   Object.cpp
 * Author: martin
 * 
 * Created on 12. leden 2017, 14:36
 */

#include "Object.h"

Object::Object(JavaClass * javaClass) {
    this->javaClass = javaClass;
    this->fields = new Variable[javaClass->fields_count];
    for (int i = 0; i< javaClass->fields_count; i++){
        this->fields[i].type = -1;
    }
    
    this->reachable = false;
    this->age = 0;
   
}

Object::Object(const Object& orig) {
}

Object::~Object() {
    delete[] fields;
}

Variable Object::getField(int idx) {
    if (idx >= this->javaClass->fields_count) {
        cerr<<"Invalid field index!"<<endl;
        exit(EXIT_FAILURE);
    }
    return this->fields[idx];
}

void Object::setField(int idx, Variable var) {
    if (idx >= this->javaClass->fields_count) {
        cerr<<"Invalid field index!"<<endl;
        exit(EXIT_FAILURE);
    }
    this->fields[idx] = var;
}


void Object::mark(){
    if (this->reachable) return;
    this->reachable = true;
    
    this->markChildren();
}

void Object::markChildren(){
    for (int i = 0; i < this->javaClass->fields_count; i++){
        if ((this->fields[i].type == OBJECTVAR)){            
            if (this->fields[i].value.objectVar !=NULL)
                this->fields[i].value.objectVar->mark();           
        }
    }
}