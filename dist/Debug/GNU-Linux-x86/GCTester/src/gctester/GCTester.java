/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gctester;

/**
 *
 * @author Thomas
 */
public class GCTester {

    public static void runDummy(int i) {
        Dummy dummy = new Dummy(i--);
        dummy.print();
        if (i > 0) runDummy(i);
    }
    
    public static void main(String[] args) {
        
        int control = 20;       // kolik je potřeba držet objektů najednou
        
        for (int i = 0; i < 100; ) {
            i += control;
            
            runDummy(control);
        }
    }
    
}
