/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package amicable_numbers;

/**
 *
 * @author Thomas
 */
public class Amicable_Numbers {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int limit = 100000;     // input
        int [] sumArray = new int[limit];
        for (int i = 0; i < limit; i++) {
            sumArray[i] = 1;
        }
        
        for (int i = 2; i < limit/2; i++) {
            for (int j = 2*i; j < limit; j += i) {
                sumArray[j] += i;
            }
        }
        for (int i = 200; i < limit; i++) {
            if (sumArray[i] <= limit && sumArray[i] >= i && i == sumArray[sumArray[i]] ) myJava.io.IOoperations.println("" + i + " " + sumArray[i]);
        }
        
    }
    
}
