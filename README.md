**MI-RUN
**

Martin Zavadil, Tomáš Tvrz

**Java napsaná v C/C++
**

Parser psaný v C, samotná implemetace je napsaná v C++.

ClassHeap je reprezentována polem ukazatelů na struktury typu JavaClass. Každá instance JavaClass obsahuje pole všech fieldů, metod a atributů a navíc ještě extra pole statických fieldů.

ObjectHeap je reprezentována dvěma poli (old a young) ukazatelů na objekty typu Object, Array a String. Garbage collector je tedy generační collector kde obě generace jsou uklízeny hybridním mark & sweep, při kterém zároveň probíhá defragmentace. 

Framestack je reprezentovaný polem ukazatelů na objekty typu Frame. Instance Frame obsahují operační zásobník a pole lokálních hodnot.

Hlavní řídící třídou je Execution, ve které jsou umístěny odkazy na všechny předešlé třídy a zároveň v ní probíhá vyhodnocování instrukcí.


Implementace využívá složky lib, ve které jsou umístěny soubory java/lang/Object, java/lang/String a java/lang/StringBuilder s Native metodami, které zprostředkovávají operace s řetězci a soubor myJava/io/IOoperations s Native metodami k obsluze vstupu a výstupu.

Pro vstup a výstup se používají zcela nové Native metody:

* myJava.io.IOoperations.readLn() - přečte řádek z konzole a vrací jej jako string

* myJava.io.IOoperations.readInt() - přečte celé číslo z konzole a vrací jej jako integer

* myJava.io.IOoperations.readChar() - přečte znak z konzole a vrací jej jako char

* myJava.io.IOoperations.println(string) - vypisuje string na konzoli.

Metody typů String a StringBuilder jsou buď napsány jako Native metody, které se chovají přesně jako originál, nebo jsou vynechány. U Stringu jsou to charAt(int) a length(), u Stringbuilderu jsou to append() a toString().


**Postup spuštění Java programu
**

* Je zapotřebí distribuce Linux s nainstalovanou Javou

* spustit Java kompilátor pomocí - javac *.java

* ke zpompilovým souborům přidat složku lib se soubory s Native metodami

* spustit pomocí ./mi-run main.class *.class , kde main soubor musí být vždy zvlášť na prvním místě