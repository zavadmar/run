/* 
 * File:   Array.h
 * Author: martin
 *
 * Created on 18. leden 2017, 11:47
 */

#ifndef ARRAY_H
#define	ARRAY_H
#include "Object.h"

class Array : public Object{
public:
    Array();
    Array(unsigned char type, int count, JavaClass * javaClass);
    Array(const Array& orig);
    Variable getField(int idx);
    void setField(int idx, Variable var);
    virtual ~Array();
    bool isArray() {return true;}
    void markChildren();
private:
    int count;
    unsigned char type;
    
    
};

#endif	/* ARRAY_H */

