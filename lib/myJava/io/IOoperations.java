/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package myJava.io;

/**
 *
 * @author martin
 */
public class IOoperations {
    public static native void println(String s);
    public static native String readln();
    public static native char readChar();
    public static native int readInt();
   
}
