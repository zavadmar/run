#ifndef PRINT_H
#define PRINT_H
#include "JavaClass.h"

#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
void print_class(FILE *stream, const JavaClass *javaClass);
#endif //PRINT_H