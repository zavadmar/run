/* 
 * File:   NativeMethods.h
 * Author: martin
 *
 * Created on 18. leden 2017, 16:53
 */

#ifndef NATIVEMETHODS_H
#define	NATIVEMETHODS_H
#include "Frame.h"
class NativeMethods {
public:
     
    NativeMethods();
    NativeMethods(const NativeMethods& orig);
    virtual ~NativeMethods();
    void println(Frame * frame);
    String readln(Frame * frame);
    char readChar(Frame * frame);
    int readInt(Frame * frame);
    
    String  stringBuilderAppendInt(Frame * frame);
    String stringBuilderAppendString(Frame * frame);
    String stringBuilderAppendChar(Frame * frame);
   // String stringBuilderAppendChar(Frame * frame);
    String stringBuilderAppendFloat(Frame * frame);
    String stringBuilderAppendBoolean(Frame * frame);
    
    Variable stringBuilderToString(Frame * frame);
    char charAt(Frame * frame);
    int stringLen(Frame * frame);
    
private:

};

#endif	/* NATIVEMETHODS_H */

