/* 
 * File:   NativeMethods.cpp
 * Author: martin
 * 
 * Created on 18. leden 2017, 16:53
 */

#include "NativeMethods.h"
#include "opcodes.h"
#include "Object.h"

#include <iostream>
#include <cstring>
#include  <sstream>
using namespace std;

NativeMethods::NativeMethods() {
}

NativeMethods::NativeMethods(const NativeMethods& orig) {
}

NativeMethods::~NativeMethods() {
}

 void NativeMethods::println(Frame * frame){
     if (frame->getLocal(0).type == STRINGVAR)
        cout<<frame->getLocal(0).value.stringVar.value<<endl;
     else if (frame->getLocal(0).type == OBJECTVAR)
                cout<<frame->getLocal(0).value.objectVar->getField(0).value.stringVar.value<<endl;
}

 
String NativeMethods::readln(Frame * frame){
    string input;
    getline(cin,input);
    
    String s;
    s.length = input.size();
    s.value = (char*)malloc(sizeof(char) * input.size() + 1);
   
    strcpy(s.value,input.c_str());
    
    return s;
}
 
char NativeMethods::readChar(Frame * frame){
    string input;
    getline(cin,input);
    
    if (input.size() != 1) {
        cerr<<"Invalid character!"<<endl;
        exit(EXIT_FAILURE);
    }    

    return input.at(0);
}
int NativeMethods::readInt(Frame * frame){
   string input;
    getline(cin,input);
    
    stringstream mystream(input.c_str());
    int retVal;
   
    if ( mystream >> retVal) {
         return retVal;                
    }    
     cerr<<"Invalid number!"<<endl;  
     exit(EXIT_FAILURE);
   
}

String NativeMethods::stringBuilderAppendInt(Frame * frame){
    int val = frame->getLocal(1).value.intVar;
    Variable object = frame->getLocal(0);
    if (object.type != OBJECTVAR || object.value.objectVar == NULL){
        cerr<<"NullPointerException!"<<endl;
    }
    Variable oldString = object.value.objectVar->getField(0);
    String newString;
    ostringstream temp;
    if (oldString.type == STRINGVAR){
        temp <<oldString.value.stringVar.value;
        free(oldString.value.stringVar.value);
    }
     
    temp << val;
    newString.length = temp.str().size();
    newString.value = (char*)malloc(sizeof(char) * newString.length + 1);
    strcpy(newString.value,temp.str().c_str());
 
    return newString;
}

String NativeMethods::stringBuilderAppendString(Frame * frame){
    String val;
     if (frame->getLocal(1).type == STRINGVAR){
         val = frame->getLocal(1).value.stringVar;        
     }
        
     else if (frame->getLocal(1).type == OBJECTVAR)
                val =frame->getLocal(1).value.objectVar->getField(0).value.stringVar;
     
    Variable object = frame->getLocal(0);
    if (object.type != OBJECTVAR || object.value.objectVar == NULL){
        cerr<<"NullPointerException!"<<endl;
    }
    Variable oldString = object.value.objectVar->getField(0);
    String newString;
    ostringstream temp;
    if (oldString.type == STRINGVAR){
        temp <<oldString.value.stringVar.value;
        free(oldString.value.stringVar.value);
    }
    
    
    
    temp<< val.value;
    newString.length = temp.str().size();
    newString.value = (char*)malloc(sizeof(char) * newString.length + 1);
    strcpy(newString.value,temp.str().c_str());   
    return newString;
}

String NativeMethods::stringBuilderAppendChar(Frame * frame){
    char val = frame->getLocal(1).value.charVar;
    Variable object = frame->getLocal(0);
    if (object.type != OBJECTVAR || object.value.objectVar == NULL){
        cerr<<"NullPointerException!"<<endl;
    }
    Variable oldString = object.value.objectVar->getField(0);
    String newString;
    ostringstream temp;
    if (oldString.type == STRINGVAR){
        temp <<oldString.value.stringVar.value;
        free(oldString.value.stringVar.value);
    }
    
    
    
    temp<< val;
    newString.length = temp.str().size();
    newString.value = (char*)malloc(sizeof(char) * newString.length + 1);
    strcpy(newString.value,temp.str().c_str());   
    return newString;
}


String NativeMethods::stringBuilderAppendFloat(Frame * frame){
    float val = frame->getLocal(1).value.floatVar;
    Variable object = frame->getLocal(0);
    if (object.type != OBJECTVAR || object.value.objectVar == NULL){
        cerr<<"NullPointerException!"<<endl;
    }
    Variable oldString = object.value.objectVar->getField(0);
    String newString;
    ostringstream temp;
    if (oldString.type == STRINGVAR){
        temp <<oldString.value.stringVar.value;
        free(oldString.value.stringVar.value);
    }
    
    
    
    temp<< val;
    newString.length = temp.str().size();
    newString.value = (char*)malloc(sizeof(char) * newString.length + 1);
    strcpy(newString.value,temp.str().c_str());   
    return newString;
}


String NativeMethods::stringBuilderAppendBoolean(Frame * frame){
    bool val = frame->getLocal(1).value.charVar;
    Variable object = frame->getLocal(0);
    if (object.type != OBJECTVAR || object.value.objectVar == NULL){
        cerr<<"NullPointerException!"<<endl;
    }
    Variable oldString = object.value.objectVar->getField(0);
    String newString;
    ostringstream temp;
    if (oldString.type == STRINGVAR){
        temp <<oldString.value.stringVar.value;
        free(oldString.value.stringVar.value);
    }
    
    
    if (val == 0)
      temp<<"false";
    else
        temp<<"true";
    //temp<< (val? "true":"false");
    newString.length = temp.str().size();
    newString.value = (char*)malloc(sizeof(char) * newString.length + 1);
    strcpy(newString.value,temp.str().c_str());   
    return newString;
}

Variable NativeMethods::stringBuilderToString(Frame * frame){
   
    Variable object = frame->getLocal(0);
    if (object.type != OBJECTVAR || object.value.objectVar == NULL){
        cerr<<"NullPointerException!"<<endl;
    }
    return object.value.objectVar->getField(0);
    
}

char NativeMethods::charAt(Frame * frame){
    if (frame->getLocal(0).type == STRINGVAR)        {
        if (frame->getLocal(1).value.intVar <  frame->getLocal(0).value.stringVar.length) 
                return frame->getLocal(0).value.stringVar.value[frame->getLocal(1).value.intVar];        
    }
     else if (frame->getLocal(0).type == OBJECTVAR)
              if (frame->getLocal(1).value.intVar <  frame->getLocal(0).value.objectVar->getField(0).value.stringVar.length) 
                return frame->getLocal(0).value.objectVar->getField(0).value.stringVar.value[frame->getLocal(1).value.intVar];        
     cerr<<"Array index out of bounds!"<<endl;
     exit(EXIT_FAILURE);
}
int NativeMethods::stringLen(Frame * frame){
     if (frame->getLocal(0).type == STRINGVAR)        {
         return frame->getLocal(0).value.stringVar.length;        
    }
     else if (frame->getLocal(0).type == OBJECTVAR)
         return frame->getLocal(0).value.objectVar->getField(0).value.stringVar.length;  
}
