/* 
 * File:   Execution.h
 * Author: martin
 *
 * Created on 10. leden 2017, 16:32
 */

#ifndef EXECUTION_H
#define	EXECUTION_H
#include "Frame.h"
#include "ClassHeap.h"
#include "ObjectHeap.h"
#include "NativeMethods.h"
class ClassHeap;
class ObjectHeap;

class Execution {
public:
    Execution(){}
    Execution(ClassHeap * classHeap,Frame * frame);
    Execution(const Execution& orig);
    virtual ~Execution();
    int parseCode(JavaClass *javaClass,Method * method, char * codePtr);
    int Execute(Frame *frame);
    void createNewArray(Frame *frame);
    void createNewAArray(Frame *frame);
    int ExecuteInvokeVirtual(Frame * frame, int type);
    bool createNewObject(Frame * frame);
    int GetMethodParametersCount(String strMethodDesc);
    void putField(Frame *frame);
    void getField(Frame * frame);
    void putStatic(Frame *frame);
    void getStatic(Frame * frame);
    bool isReturnSmt(String s);
    void ExecuteNative(Frame * frame,const char * methodName);
    
private:
    ClassHeap *classHeap;
    ObjectHeap *objectHeap;
    Frame ** frameStack;
    int frameCnt;
    int frameMax;
    NativeMethods* native;
};

#endif	/* EXECUTION_H */

