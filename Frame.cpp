/* 
 * File:   Frame.cpp
 * Author: martin
 * 
 * Created on 10. leden 2017, 15:33
 */

#include "Frame.h"
#include "JavaClass.h"
#include "Execution.h"
#include <cstring>
using namespace std;

Frame::Frame(JavaClass* jclass, Method* method){
    this->javaClass = jclass;
    this->method = method;
    
    parseCode(jclass,method);
    this->stack = new Variable[this->maxStack];
    this->locals = new Variable[this->maxLocals];
    for (int i = 0; i< this->maxLocals; i++){
        this->locals[i].type = 0;
    }
    this->pc = 0;
    this->sp = 0;    
}
Frame::Frame(JavaClass* jclass, Method* method, int maxLocals, int maxStack){
    this->javaClass = jclass;
    this->method = method;
    this->maxLocals = maxLocals;
    this->maxStack = maxStack;
    this->stack = new Variable[maxStack];
    this->locals = new Variable[maxLocals];
    for (int i = 0; i< maxLocals; i++){
        this->locals[i].type = 0;
    }
    this->pc = 0;
    this->sp = 0;    
}
Frame::~Frame() {
    delete[] stack;
    delete[] locals;
}

bool Frame::parseCode(JavaClass *javaClass,Method * method){
    
    for (int i = 0; i<method->attrs_count; i++){
        Item * item = get_item(javaClass, method->attrs[i].name_idx);
        //cout<<item->value.string.value;
       // cout<<strcmp(item->value.string.value,"Code");
      //  string s = "Code";
        if (! compareString(item->value.string,"Code")){
            char * pc = method->attrs[i].info;
          //  cout<<method->attrs[i].length<<"\n";
            //cout<<method->attrs[i].info<<"\n";
            this->maxStack = getu2(pc);
            pc+=2;
            this->maxLocals = getu2(pc);
          //  cout<<maxLocals<<"\n";
            pc+=2;
            this->codeLen = getu4(pc);
            
            //cout<< (int)pc[0]<<(int)pc[1]<<(int)pc[2]<<(int)pc[3];
            pc+=4;
            this->code = pc;
            return true;
            //cout<< codeLen<<"\n";
           // pc+=13;
           // int ecCnt = getu4(pc);
          ///  cout<<ecCnt<<endl;
           // cout<<maxStack;
            //cout<<"here";
            
        }
    }
    return false;
}