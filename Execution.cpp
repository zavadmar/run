/* 
 * File:   Execution.cpp
 * Author: martin
 * 
 * Created on 10. leden 2017, 16:32
 */

#include "Execution.h"
#include "Frame.h"
#include <cstring>
#include <iostream>
#include "opcodes.h"
#include "ObjectHeap.h"
#include "constants.h"
#include "NativeMethods.h"

using namespace std;

Execution::Execution(ClassHeap *classHeap, Frame * frame) {
    this->classHeap = classHeap;   
    this->native = new NativeMethods();
    frameMax = 100;
    frameCnt = 1;
    this->frameStack = new Frame*[frameMax]; 
    this->objectHeap = new ObjectHeap(this->frameStack,classHeap);
    frameStack[0] = frame;

}

Execution::Execution(const Execution& orig) {
}

Execution::~Execution() {
    delete native;
    delete objectHeap;
    delete[] frameStack;
     
}

int Execution::Execute(Frame *frame) {


    Variable v, a;
    long lv;
    Item * item;
    int idx;
    
    while (1) {

//        cout << dec << "Stack:" << endl;
//        for (int i = 0; i < frame->getSp(); i++) {
//            cout << i << ": type"<<frame->getStack(i).type <<" Value:";
//            frame->getStack(i).printVar();
//        }
//
//        cout << hex << (unsigned int) frame->code[frame->getPc()] << endl;
        switch ((unsigned char) frame->code[frame->getPc()]) {

            case nop:
                frame->incPc();
                break;
                
                //Instructions that push a constant onto the stack
            case iconst_m1:
                v.type = INTEGERVAR;
                v.value.intVar = -1;
                frame->push(v);                
                frame->incPc();
                break;
            case iconst_0:
            case iconst_1:
            case iconst_2:
            case iconst_3:
            case iconst_4:
            case iconst_5:                
                v.type = INTEGERVAR;
                v.value.intVar = frame->code[frame->getPc()] - iconst_0;
                frame->push(v);                
                frame->incPc();
                break;

            case aconst_null:
                v.type = OBJECTVAR;
                v.value.objectVar = NULL;
                frame->push(v);
                frame->incPc();
                break;

            case lconst_0:// 9 /*(0x9)*/ 
            case lconst_1:// 10 /*(0xa)*/
                v.type = LONGVAR;
                v.value.longVar.high = 0;
                v.value.longVar.low = ((unsigned char) frame->code[frame->getPc()] - lconst_0);
                frame->push(v);
                frame->incPc();
                break;

            case bipush:// 16 /*(0x10)*/                
                v.type = INTEGERVAR;
                v.value.intVar = (int) frame->code[frame->getPc() + 1];                
                frame->push(v);
                frame->incPc(2);
                break;
            case sipush:// 17 /*(0x11)*/
                
                v.type = INTEGERVAR;
                v.value.intVar = (int)getu2(&frame->code[frame->getPc() + 1]);
                frame->push(v);
                frame->incPc(3);
                break;

            case ldc: //Push item from constant pool 			

                item = get_item(frame->getClass(), frame->code[frame->getPc() + 1]); // frame->stack[++frame->sp] =LoadConstant(frame->pClass, (u1)bc[frame->pc+1]);

                switch (item->tag) {
                    case STRING_UTF8:
                        v.value.stringVar = item->value.string;
                        v.type = STRINGVAR;
                        break;
                    case FLOAT:
                        v.value.floatVar = item->value.flt;
                        v.type = FLOATVAR;
                        break;
                    case DOUBLE:
                        v.value.doubleVar = item->value.dbl;
                        v.type = DOUBLEVAR;
                        break;
                    case LONG:
                        v.value.longVar = item->value.lng;
                        v.type = LONGVAR;
                        break;
                    case INTEGER:
                        v.value.intVar = item->value.integer;
                        v.type = INTEGERVAR;
                        break;
                    case STRING:
                        v.value.stringVar = get_item(frame->getClass(), item->value.ref.class_idx)->value.string;
                        v.type = STRINGVAR;
                        break;
                    default: cout << "ERROR: ldc TAG = " << item->tag << endl;
                        break;
                }               
                frame->push(v);
                frame->incPc(2);
                break;

            case ldc2_w:// 20 /*(0x14)*/
                
                item = get_item(frame->getClass(), getu2(&frame->code[frame->getPc() + 1])); // frame->stack[++frame->sp] =LoadConstant(frame->pClass, (u1)bc[frame->pc+1]);

                switch (item->tag) {
                    case STRING_UTF8:
                        v.value.stringVar = item->value.string;
                        v.type = STRINGVAR;
                        break;
                    case FLOAT:
                        v.value.floatVar = item->value.flt;
                        v.type = FLOATVAR;
                        break;
                    case DOUBLE:
                        v.value.doubleVar = item->value.dbl;
                        v.type = DOUBLEVAR;
                        break;
                    case LONG:
                        v.value.longVar = item->value.lng;
                        v.type = LONGVAR;
                        break;
                    case INTEGER:
                        v.value.intVar = item->value.integer;
                        v.type = INTEGERVAR;
                        break;
                    case STRING:
                        v.value.stringVar = get_item(frame->getClass(), item->value.ref.class_idx)->value.string;
                        v.type = STRINGVAR;
                        break;
                    default: cout << "ERROR: ldc TAG = " << item->tag << endl;
                        break;
                }

                frame->push(v);
                frame->incPc(3);
                break;

                //Instructions that load a local variable onto the stack
            case aload:// 25 /*(0x19)*/
                v = frame->getLocal(frame->code[frame->getPc() + 1]);
                frame->push(v);
                frame->incPc(2);
                break;

            case iload:// 21 /*(0x15)*/
                v = frame->getLocal(frame->code[frame->getPc() + 1]);
                frame->push(v);
                frame->incPc(2);
                break;
            case iload_0: //26 Load int from local variable 0 
            case iload_1: //27 Load int from local variable 1 
            case iload_2: //28 Load int from local variable 2 
            case iload_3: //29 Load int from local variable 3 
                v = frame->getLocal(frame->code[frame->getPc()] - iload_0);
                frame->push(v);
                frame->incPc();
                break;


            case lload:// 22 /*(0x16)*/

                break;
            case lload_0:// 30 /*(0x1e) */
            case lload_1:// 31 /*(0x1f) */
            case lload_2:// 32 /*(0x20) */
            case lload_3:// 33 /*(0x21) */
                frame->push(frame->getLocal((unsigned char) frame->code[frame->getPc()] - lload_0));
                frame->incPc();
                break;

            case fload_0: // 34 /*(0x22)*/ 
            case fload_1: // 35 /*(0x23) */
            case fload_2: // 36 /*(0x24) */
            case fload_3: // 37 /*(0x25)*/
                frame->push(frame->getLocal((unsigned char) frame->code[frame->getPc()] - fload_0));
                frame->incPc();
                break;

            case aload_0: //42 Load reference from local variable 0
            case aload_1: //Load reference from local variable 1
            case aload_2: //Load reference from local variable 2
            case aload_3: //Load reference from local variable 3
                frame->push(frame->getLocal((unsigned char) frame->code[frame->getPc()] - aload_0));
                frame->incPc();
                break;
                //
            case iaload:// 46 /*(0x2e)*/Load int from array                
            case laload://47
            case faload://48 
            case daload://49 
            case aaload://50 
            case baload://51 
            case caload://52
            case saload://53                 
                idx = frame->pop().value.intVar;
                if (frame->top().type != OBJECTVAR || frame->top().value.objectVar == NULL || !frame->top().value.objectVar->isArray()) {
                    cerr << "NullPointerException! In iastore/aastore." << endl;
                    exit(EXIT_FAILURE);
                }
                frame->push(frame->pop().value.objectVar->getField(idx));
                frame->incPc();
                break;
                //Instructions that store a value from the stack into a local variable
            case astore:// 58 (0x3a)
            case istore:// 54 /*(0x36)*/
            case lstore:// 55 /*(0x37)*/
            case fstore:// 56 /*(0x37)*/
            case dstore:// 57 /*(0x37)*/
                frame->setLocal(frame->code[frame->getPc() + 1], frame->pop());
                frame->incPc(2);
                break;
            case istore_0: // 59 /*(0x3b)*/ 
            case istore_1: // 60 /*(0x3c) */
            case istore_2: // 61 /*(0x3d) */
            case istore_3: // 62 /*(0x3e)*/	
                 if (frame->top().type == CHARVAR){
                    v.type = INTEGERVAR;
                    v.value.intVar = (int) frame->pop().value.charVar;
                }
                else {
                    v = frame->pop();
                }
                frame->setLocal(frame->code[frame->getPc()] - istore_0,v);
                
                frame->incPc();
                break;

            case lstore_0: // 63 /*(0x3f) */
            case lstore_1: // 64 /*(0x40) */
            case lstore_2: // 65 /*(0x41) */
            case lstore_3: // 66 /*(0x42) */
                frame->setLocal(frame->code[frame->getPc()] - lstore_0, frame->pop());
                frame->incPc();
                break;

            case fstore_0:
            case fstore_1:
            case fstore_2:
            case fstore_3:
                frame->setLocal(frame->code[frame->getPc()] - fstore_0, frame->pop());
                frame->incPc();
                break;

            case astore_0:// 75 /*(0x4b) Store reference into local variable 0*/
            case astore_1:// 76 /*(0x4c) */
            case astore_2:// 77 /*(0x4d) */
            case astore_3:// 78 /*(0x4e)*/
                frame->setLocal(frame->code[frame->getPc()] - astore_0, frame->pop());
                frame->incPc();
                break;
                //
            case iastore:// 79 /*(0x4f)*/
            case fastore:
            case sastore:
            case castore:
            case dastore:
            case lastore:
            case bastore:
            case aastore: // 83
                v = frame->pop();
                idx = frame->pop().value.intVar;
                if (frame->top().type != OBJECTVAR || frame->top().value.objectVar == NULL || !frame->top().value.objectVar->isArray()) {
                    cout << "NullPointerException! In iastore/aastore." << endl;
                    exit(EXIT_FAILURE);
                }
                frame->pop().value.objectVar->setField(idx, v);
                frame->incPc();
                break;
                
            case dup:// 89 /*(0x59)*/
                frame->push(frame->top());
                frame->incPc();
                break;
            case dup2:// 92 /*(0x5c)*/
                frame->push(frame->peek());
                frame->push(frame->peek());
                //frame->push(frame->top());
                
                frame->incPc();
                break;
            case dup_x1:// 90 /*(0x5a)*/
                a = frame->pop();
                v = frame->pop();
                frame->push(a);
                frame->push(v);
                frame->push(a);
                frame->incPc();
                break;

                //Integer Arithmetic 
            case iadd: //96
                v.type = INTEGERVAR;                
                v.value.intVar = frame->pop().returnInt() + frame->pop().returnInt();               
                frame->push(v);
                frame->incPc();
                break;
            case ladd:// 97 /*(0x61)*/            
                lv = ((long) frame->top().value.longVar.high << 32 | (long) frame->pop().value.longVar.low) + ((long) frame->top().value.longVar.high << 32 | (long) frame->pop().value.longVar.low);
                v.value.longVar.high = (int) lv >> 32;
                v.value.longVar.low = (int) lv & (0xFFFFFFFF);
                frame->push(v);
                frame->incPc();
                break;
            case isub: //100
                v.type = INTEGERVAR;
                v.value.intVar = 0; 
                v.value.intVar -= frame->pop().value.intVar;            
                v.value.intVar += frame->pop().value.intVar;              
                frame->push(v);
                frame->incPc();
                break;
            case imul://104
                v.type = INTEGERVAR;
                v.value.intVar = frame->pop().value.intVar * frame->pop().value.intVar;
                frame->push(v);
                frame->incPc();
                break;
            case idiv: //100
                v.type = INTEGERVAR;
                v.value.intVar = 0; 
                if (frame->top().value.intVar == 0){
                    cerr<<"Arithmetic exception!"<<endl;
                    exit(EXIT_FAILURE);
                }
                v.value.intVar = frame->pop().value.intVar;            
                v.value.intVar = frame->pop().value.intVar / v.value.intVar;              
                frame->push(v);
                frame->incPc();
                break;    
            case iinc:// 132 /*(0x84)*/ Increment local variable by constant
                v.type = INTEGERVAR;
                v.value.intVar = frame->getLocal(frame->code[frame->getPc() + 1]).value.intVar + (char) frame->code[frame->getPc() + 2];
            //    cout << "local inc: " << v.value.intVar << endl;
                frame->setLocal(frame->code[frame->getPc() + 1], v);
                frame->incPc(3);
                break;                           
                             
                //Instructions that deal with objects

            case _new:// 187 (0xbb)
                createNewObject(frame);
                frame->incPc(3);
                break;
            case putfield: //181 (0xb5): Set field in object
                putField(frame);
                //frame->sp-=2;
                frame->incPc(3);
                break;
                
            case getfield: //180 (0xb4) Fetch field from object
                getField(frame);
                frame->incPc(3);
                break;

            case putstatic: //179 (0xb3): Set field in object
                putStatic(frame);                
                frame->incPc(3);
                break;
                
            case getstatic: //178 (0xb2) Fetch field from object
                getStatic(frame);
                frame->incPc(3);
                break;
                
            case newarray:// 188 /*(0xbc)*/
                createNewArray(frame);
                frame->incPc(2);
                break;

            case anewarray: //189
                    createNewAArray(frame);
                    frame->incPc(3);
                    break;                

                //Conditional branch instructions

            case if_icmpeq: // 159 /*(0x9f) */
                if (frame->pop().value.intVar == frame->pop().value.intVar) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }
                break;
                
            case if_icmpne: //160 /*(0xa0) */
                if (frame->pop().value.intVar != frame->pop().value.intVar) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }
                break;
                
            case if_icmplt: // 161 /*(0xa1) */
                if (frame->pop().value.intVar > frame->pop().value.intVar) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
            case if_icmpge: // 162 /*(0xa2) */
                if (frame->pop().value.intVar <= frame->pop().value.intVar) {

                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }
                break;
                
            case if_icmpgt: // 163 /*(0xa3) */
                if (frame->pop().value.intVar < frame->pop().value.intVar) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case if_icmple: // 164 /*(0xa4)*/
                if (frame->pop().value.intVar >= frame->pop().value.intVar) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }
                break;
                
            case ifeq:// 153 /*(0x99) */
                if (frame->pop().value.intVar == 0) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
             case ifnull:// 198 /*(0xc6) */
                if (frame->pop().value.objectVar == NULL) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case ifne:// 154 /*(0x9a) */
                if (frame->pop().value.intVar != 0) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case iflt:// 155 /*(0x9b) */
                if (frame->pop().value.intVar < 0) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case ifge:// 156 /*(0x9c) */
                if (frame->pop().value.intVar >= 0) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case ifgt:// 157 /*(0x9d) */
                if (frame->pop().value.intVar > 0) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case ifle:// 158 /*(0x9e)*/
                if (frame->pop().value.intVar <= 0) {
                    frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                } else {
                    frame->incPc(3);
                }

                break;
                
            case _goto: // 167 /*(0xa7)*/
                frame->incPc((short) getu2(&frame->code[frame->getPc() + 1]));
                break;
                
                //Method invocation instructions
            case invokespecial:
                ExecuteInvokeVirtual(frame, invokespecial);
                frame->incPc(3);
                break;

            case invokevirtual: //182
                ExecuteInvokeVirtual(frame, invokevirtual);
                frame->incPc(3);
                break;

            case invokestatic:// 184 
                ExecuteInvokeVirtual(frame, invokestatic);
                frame->incPc(3);
                break;
                //Method return instructions
            case ireturn: //172 (0xac)			
                //			DbgPrint(_T("----IRETURN------\n"));
               // cout << "----IRETURN------\n";
                frame->setStack(0, frame->top());
                return ireturn;
                break;
            case areturn: //172 (0xac)			
                
               // cout << "----ARETURN------\n";
                frame->setStack(0, frame->top());
                return areturn;
                break;
            case _return: //177 (0xb1): Return (void) from method			
               // cout << "----RETURN------\n";
                return 0; 
                break;
                
            default:
                //error=1;
                cout << "------DEFAULT-----" << (int) frame->code[frame->getPc()];
                exit(EXIT_FAILURE);
                break;

        }

    }

}

int Execution::parseCode(JavaClass *javaClass, Method * method, char * codePtr) {

    for (int i = 0; i < method->attrs_count; i++) {
        Item * item = get_item(javaClass, method->attrs[i].name_idx);
        
        if (!strcmp(item->value.string.value, "Code")) {
            char * pc = method->attrs[i].info;
            
            short maxStack = getu2(pc);
            pc += 2;
            short maxLocals = getu2(pc);
            
            pc += 2;
            int codeLen = getu4(pc);
            
            pc += 4;
            codePtr = pc;
            return codeLen;
            

        }
    }
}

int Execution::ExecuteInvokeVirtual(Frame * frame, int type) {

    unsigned short methodIndex = getu2(&frame->code[frame->getPc() + 1]);
    Item * method = get_item(frame->getClass(), methodIndex);
    if (method->tag != CONSTANT_Methodref) return -1;

    Item * jclassIdx = get_item(frame->getClass(), method->value.ref.class_idx);
    if (jclassIdx->tag != CONSTANT_Class) return -1;

    Item * className = get_item(frame->getClass(), jclassIdx->value.integer);

    JavaClass * newJavaClass = this->classHeap->getClass(className->value.string);

    Item * methodNameAndTypeIdx = get_item(frame->getClass(), method->value.ref.name_idx);

    if (methodNameAndTypeIdx->tag != CONSTANT_NameAndType) return -1;

    Item * methodName = get_item(frame->getClass(), methodNameAndTypeIdx->value.ref.class_idx);
    Item * methodDesc = get_item(frame->getClass(), methodNameAndTypeIdx->value.ref.name_idx);
   
    int methodIdx = getMethodIdxFromClass(newJavaClass, methodName->value.string, methodDesc->value.string);
    Frame *newFrame;
   // cout<<"++++++++Starting: "<<className->value.string.value<<":"<<methodName->value.string.value<<endl;
    int paramsCnt = GetMethodParametersCount(methodDesc->value.string);
    if (type != invokestatic) paramsCnt++;
 
    if (newJavaClass->methods[methodIdx].flags & ACC_NATIVE){
         newFrame = new Frame(newJavaClass, &newJavaClass->methods[methodIdx],paramsCnt,1);    
    }
    else{
         newFrame = new Frame(newJavaClass, &newJavaClass->methods[methodIdx]);    
    }
    this->frameStack[this->frameCnt] = newFrame;
    this->frameCnt++;

    // copy params
   

    
   
    for (int i = frame->getSp() - paramsCnt; i < frame->getSp(); i++) {       
            newFrame->setLocal(i - (frame->getSp() - paramsCnt), frame->getStack(i));        
        }    
 
    if (newFrame->getMethod()->flags & ACC_NATIVE){
        char * methodNameString = new char[className->value.string.length+methodName->value.string.length+methodDesc->value.string.length+2];
        strcpy(methodNameString,className->value.string.value);
        methodNameString[className->value.string.length]='@';
        strcpy(methodNameString+className->value.string.length+1,methodName->value.string.value);
        strcpy(methodNameString+className->value.string.length+1+methodName->value.string.length,methodDesc->value.string.value);
       cout<<methodNameString<<endl;
        ExecuteNative(newFrame,methodNameString);
        delete[] methodNameString;
    }
    else this->Execute(newFrame);


    //delete old params

    frame->decSp(paramsCnt);
    //return
    //cout<<"Return"<<endl;
    if (isReturnSmt(methodDesc->value.string)) {
        frame->push(newFrame->getStack(0));
        
    }

    
    this->frameCnt--;
    delete this->frameStack[this->frameCnt];
    this->frameStack[this->frameCnt] = NULL;


}

int Execution::GetMethodParametersCount(String strMethodDesc) {
    int count = 0;

    int i, len = strMethodDesc.length;
  
    for (i = 1; i < len; i++) {
        if (strMethodDesc.value[i] == 'L') {
            while (strMethodDesc.value[i] != ';') i++;
        }
        if (strMethodDesc.value[i] == ')') break;
        //if (strMethodDesc.value[i] == 'J' || strMethodDesc.value[i] == 'D')
       //     count++;
        count++;
    }

    return count;
}

bool Execution::createNewObject(Frame * frame) {
    Variable v;
    v.type = OBJECTVAR;
    short classIdx =  getu2(&frame->code[frame->getPc() + 1]);
    Item * item = get_item(frame->getClass(),classIdx);
    Item * className = get_item(frame->getClass(), item->value.ref.class_idx);
    v.value.objectVar = this->objectHeap->createObject(classHeap->getClass(className->value.string),frameCnt); // pointer vs index
    frame->push(v);
}

void Execution::createNewArray(Frame *frame) {
    Variable v;
    v.type = OBJECTVAR;
    v.value.objectVar = this->objectHeap->createArray((frame->code[frame->getPc() + 1]), frame->pop().value.intVar, frameCnt);
    frame->push(v);
}

void Execution::createNewAArray(Frame *frame) {
    Variable v;
    v.type = OBJECTVAR;
    short idx = getu2(&frame->code[frame->getPc() + 1]);
    Item * item = get_item(frame->getClass(), idx);
    Item * className = get_item(frame->getClass(), item->value.ref.class_idx);
    JavaClass * javaClass = this->classHeap->getClass(className->value.string);
    v.value.objectVar = this->objectHeap->createAArray(javaClass, frame->pop().value.intVar,frameCnt);
    frame->push(v);
}

void Execution::putField(Frame *frame) {
    Variable value = frame->pop();
    Variable objectRef = frame->pop();
    if (objectRef.value.objectVar == NULL) {
        cerr << "NULLPointerException!" << endl;
        exit(EXIT_FAILURE);
    }
    unsigned short fieldRef = getu2(&frame->code[frame->getPc() + 1]);
    Item *item = get_item(frame->getClass(), fieldRef);
    Item * fieldNameAndDescIdx = get_item(frame->getClass(), item->value.ref.name_idx);
    Item * fieldNameStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.class_idx);
    Item * fieldDescStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.name_idx);

    int fieldIdx = getFieldIdxFromClass(objectRef.value.objectVar->getJavaClass(), fieldNameStr->value.string, fieldDescStr->value.string);
    objectRef.value.objectVar->setField(fieldIdx, value);
}

void Execution::getField(Frame * frame) {
    Variable objectRef = frame->pop();
    if (objectRef.value.objectVar == NULL) {
        cerr << "NULLPointerException!" << endl;
        exit(EXIT_FAILURE);
    }
    unsigned short fieldRef = getu2(&frame->code[frame->getPc() + 1]);

    Item *item = get_item(frame->getClass(), fieldRef);
    Item * fieldNameAndDescIdx = get_item(frame->getClass(), item->value.ref.name_idx);
    Item * fieldNameStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.class_idx);
    Item * fieldDescStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.name_idx);

    int fieldIdx = getFieldIdxFromClass(objectRef.value.objectVar->getJavaClass(), fieldNameStr->value.string, fieldDescStr->value.string);

  
  
    frame->push(objectRef.value.objectVar->getField(fieldIdx));
}

void Execution::putStatic(Frame *frame) {

    Variable value = frame->pop();
    unsigned short fieldRef = getu2(&frame->code[frame->getPc() + 1]);
  
    Item *item = get_item(frame->getClass(), fieldRef);  
    Item * fieldClassIdx = get_item(frame->getClass(), item->value.ref.class_idx);
    Item * fieldClassName = get_item(frame->getClass(), fieldClassIdx->value.ref.class_idx);
    Item * fieldNameAndDescIdx = get_item(frame->getClass(), item->value.ref.name_idx);  
    Item * fieldNameStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.class_idx);  
    Item * fieldDescStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.name_idx);
  

    JavaClass *jClass = classHeap->getClass(fieldClassName->value.string);
 
 
    int fieldIdx = getFieldIdxFromClass(jClass, fieldNameStr->value.string, fieldDescStr->value.string);
  
    jClass->staticFields[fieldIdx] = value;
}

void Execution::getStatic(Frame * frame) {
    
    unsigned short fieldRef = getu2(&frame->code[frame->getPc() + 1]);
     Item *item = get_item(frame->getClass(), fieldRef);  
    Item * fieldClassIdx = get_item(frame->getClass(), item->value.ref.class_idx);
    Item * fieldClassName = get_item(frame->getClass(), fieldClassIdx->value.ref.class_idx);
    Item * fieldNameAndDescIdx = get_item(frame->getClass(), item->value.ref.name_idx);  
    Item * fieldNameStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.class_idx);  
    Item * fieldDescStr = get_item(frame->getClass(), fieldNameAndDescIdx->value.ref.name_idx);

    JavaClass *jClass = classHeap->getClass(fieldClassName->value.string);
    int fieldIdx = getFieldIdxFromClass(jClass, fieldNameStr->value.string, fieldDescStr->value.string);
  
    frame->push(jClass->staticFields[fieldIdx]);
}

bool Execution::isReturnSmt(String s) {
    for (int i = 0; i < s.length; i++) {
        if (s.value[i] == ')')
            if (s.value[i + 1] == 'V')
                return false;
    }
    return true;
}

void Execution::ExecuteNative(Frame * frame, const char * methodName){
    
    Variable retVal;
    Variable stringVar;
   
    if (! strcmp(methodName,"myJava/io/IOoperations@println(Ljava/lang/String;)V")){
         native->println(frame);
    }
    else
    if  (! strcmp(methodName,"myJava/io/IOoperations@readln()Ljava/lang/String;")){
            retVal.type = OBJECTVAR;
            retVal.value.objectVar = objectHeap->createString(frameCnt);
            stringVar.type = STRINGVAR;    
            stringVar.value.stringVar = native->readln(frame);
            retVal.value.objectVar->setField(0,stringVar);
           // cout<<"RETVAL"<<retVal.value.stringVar.value<<endl;
            frame->setStack(0,retVal);
    }
    else if(! strcmp(methodName,"myJava/io/IOoperations@readChar()C")){
            retVal.type = CHARVAR;
            retVal.value.charVar = native->readChar(frame);
         
            frame->setStack(0,retVal);
    }
     else if(! strcmp(methodName,"myJava/io/IOoperations@readInt()I")){
            retVal.type = INTEGERVAR;
            retVal.value.intVar = native->readInt(frame);
        
            frame->setStack(0,retVal);
    }
     else if(! strcmp(methodName,"java/lang/StringBuilder@append(I)Ljava/lang/StringBuilder;")){
           
            retVal = frame->getLocal(0);
            Variable v;
            v.type = STRINGVAR;
            v.value.stringVar = native->stringBuilderAppendInt(frame);
            retVal.value.objectVar->setField(0,v);
        
            frame->setStack(0,retVal);
    }
    else if(! strcmp(methodName,"java/lang/StringBuilder@append(Ljava/lang/String;)Ljava/lang/StringBuilder;")){
            retVal = frame->getLocal(0);
            Variable v;
            v.type = STRINGVAR;
            v.value.stringVar = native->stringBuilderAppendString(frame);
            retVal.value.objectVar->setField(0,v);
        
            frame->setStack(0,retVal);
    }   
     else if(! strcmp(methodName,"java/lang/StringBuilder@append(C)Ljava/lang/StringBuilder;")){
            retVal = frame->getLocal(0);
            Variable v;
            v.type = STRINGVAR;
            v.value.stringVar = native->stringBuilderAppendChar(frame);
            retVal.value.objectVar->setField(0,v);
          
            frame->setStack(0,retVal);
    }   
     else if(! strcmp(methodName,"java/lang/StringBuilder@append(F)Ljava/lang/StringBuilder;")){
            retVal = frame->getLocal(0);
            Variable v;
            v.type = STRINGVAR;
            v.value.stringVar = native->stringBuilderAppendFloat(frame);
            retVal.value.objectVar->setField(0,v);
          
            frame->setStack(0,retVal);
    }   
     else if(! strcmp(methodName,"java/lang/StringBuilder@append(Z)Ljava/lang/StringBuilder;")){
            retVal = frame->getLocal(0);
            Variable v;
            v.type = STRINGVAR;
            v.value.stringVar = native->stringBuilderAppendBoolean(frame);
            retVal.value.objectVar->setField(0,v);
          
            frame->setStack(0,retVal);
    }   
    else if(! strcmp(methodName,"java/lang/StringBuilder@toString()Ljava/lang/String;")){
            retVal.type = OBJECTVAR;
            retVal.value.objectVar = objectHeap->createString(frameCnt);
                    
            stringVar = native->stringBuilderToString(frame);
          
            
            retVal.value.objectVar->setField(0,stringVar);
         //   cout<<"strinhVar"<<retVal.value.objectVar->getField(0).value.stringVar.value<<endl;
            frame->setStack(0,retVal);
    }
    else if(! strcmp(methodName,"java/lang/String@charAt(I)C")){
            retVal.type = CHARVAR;                                            
            retVal.value.charVar =  native->charAt(frame);
            
            frame->setStack(0,retVal);
    }
    else if(! strcmp(methodName,"java/lang/String@length()I")){
            retVal.type = INTEGERVAR;                                            
            retVal.value.intVar =  native->stringLen(frame);
        
            frame->setStack(0,retVal);
    }
   
                
        
    
   
}
