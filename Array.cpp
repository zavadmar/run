/* 
 * File:   Array.cpp
 * Author: martin
 * 
 * Created on 18. leden 2017, 11:47
 */

#include "Array.h"

Array::Array() {

}

Array::Array(unsigned char type, int count, JavaClass * javaCLass) {
    if (count < 0) {
        cout << "NegativeArraySizeException!";
        exit(EXIT_FAILURE);
    }
    this->javaClass = javaCLass;
    this->reachable = false;
    this->age = 0;
    this->count = count;
    this->type = type;
    this->fields = new Variable[count];

    bool isObject = (type == 0);
    Variable v;
    switch (type) {
        case 0: v.type = OBJECTVAR;
            v.value.objectVar = NULL;
            break;
        case 4: v.type = CHARVAR;
            v.value.charVar = 0;
            break;
        case 5: v.type = CHARVAR;
            v.value.charVar = '\0';
            break;
        case 6: v.type = FLOATVAR;
            v.value.floatVar = 0;
            break;
        case 7: v.type = DOUBLEVAR;
            v.value.doubleVar.high = 0;
            v.value.doubleVar.low = 0;
            break;
        case 8: v.type = CHARVAR;
            v.value.charVar = 0;
            break;
        case 9: v.type = SHORTVAR;
            v.value.shortVar = 0;
            break;
        case 10: v.type = INTEGERVAR;
            v.value.intVar = 0;
            break;
        case 11: v.type = LONGVAR;
            v.value.doubleVar.high = 0;
            v.value.doubleVar.low = 0;
            break;
        default: v.type = OBJECTVAR;
            v.value.objectVar = NULL;
            break;
    }

    for (int i = 0; i < count; i++) {
        this->fields[i] = v;

    }
}

Array::Array(const Array& orig) {
}

Array::~Array() {
}

Variable Array::getField(int idx) {
    if (idx >= this->count) {
        cout << "ArrayIndexOutOfBoundException!";
        exit(EXIT_FAILURE);
    }
    return fields[idx];
}

void Array::setField(int idx, Variable var) {
    if (idx >= this->count) {
        cout << "ArrayIndexOutOfBoundException!";
        exit(EXIT_FAILURE);
    }
    fields[idx] = var;
}

void Array::markChildren() {
    if (this->javaClass != NULL)
        for (int i = 0; i < this->count; i++) {
            if ((this->fields[i].type == OBJECTVAR)) {
                if (this->fields[i].value.objectVar != NULL)
                    this->fields[i].value.objectVar->mark();
            }
        }
}