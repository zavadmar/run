#include "JavaClass.h"
#include <endian.h>
#include <errno.h>
#include "PrintJavaCLass.h"
#include "Execution.h"
#include "Frame.h"
#include "ClassHeap.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <iostream>

using namespace std;


//void Execute(JavaClass* javaClass){
//    Frame *frame = new Frame(javaClass, &javaClass->methods[getMethodIdxFromClass(javaClass, "main", "([Ljava/lang/String;)V")]);
//    Item * item = get_item(javaClass,frame->getMethod()->name_idx);
//    cout << item->value.string.value << "\n";
//    Execution *execution = new Execution(heap,frame);
//    //char * codePtr;
//    //execution->parseCode(javaClass,frame->getMethod(),codePtr);
//    execution->Execute(frame);
//}
void Execute(ClassHeap* heap){
    JavaClass *javaClass = heap->getClass(0);
    Frame *frame = new Frame(javaClass, &javaClass->methods[getMethodIdxFromClass(javaClass, "main", "([Ljava/lang/String;)V")]);

    Execution *execution = new Execution(heap,frame);
    heap->clinitAll(execution);
  
    execution->Execute(frame);
    delete frame;
    delete execution;
}

JavaClass * readClass(string className){
    JavaClass * jClass;
        const char *file_name = className.c_str();
        FILE *file = fopen(file_name, "r");
        if (!file) {
            printf("Could not open '%s': %s\n", file_name, strerror(errno));
              return NULL;;
        }
        // Check the file header for .javaClass nature
        if (!is_class(file)) {
            printf("Skipping '%s': not a valid javaClass file\n", file_name);
           return NULL;
        }
        const ClassFile class_file = {
            file_name,
            file
        };
        JavaClass *javaClass = read_class(class_file);
        
        if (javaClass == NULL) {
         fprintf(stderr, "Parsing aborted; invalid javaClass file contents: %s\n", class_file.file_name);
        } else {
         
            jClass = javaClass;
         
        // yay, valid!        
        }                      
        fclose(file);
        return jClass;
}

int main(int argc, char *args[]) {
    ClassHeap *heap = new ClassHeap();
    if (argc == 1) {
        printf("Please pass at least 1 .class file to open");
        exit(EXIT_FAILURE);
    }
    int i;
    for (i = 1; i < argc; i++) {   
            heap->addClass(readClass(args[i]));                                                            
    }
    heap->addClass(readClass("./lib/java/lang/Object.class"));
    heap->addClass(readClass("./lib/java/lang/String.class"));
    heap->addClass(readClass("./lib/java/lang/StringBuilder.class"));
    heap->addClass(readClass("./lib/myJava/io/IOoperations.class"));
      Execute(heap);
      delete heap;
      
    exit(EXIT_SUCCESS);
}

