/* 
 * File:   StringObject.cpp
 * Author: martin
 * 
 * Created on 23. leden 2017, 11:57
 */

#include "StringObject.h"
#include "Object.h"

StringObject::StringObject() {
    this->javaClass = NULL;
    this->fields = new Variable[1];
   
    this->fields[0].type = STRINGVAR;
    
    this->reachable = false;
    this->age = 0;
}

void StringObject::setField(int idx, Variable var){
    idx = 0;//if (idx > 1 ) return;
    this->fields[idx] = var;
}
Variable StringObject::getField(int idx){   
    return this->fields[0];
}

StringObject::StringObject(const StringObject& orig) {
}

StringObject::~StringObject() {
    free(this->fields[0].value.stringVar.value);
 //   delete[] this->fields;
}

