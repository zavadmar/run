#ifndef CLASS_H
#define CLASS_H
#include <endian.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string>
#include <iostream>
#define u2 uint16_t
#define u4 uint32_t
#define getu4(p) (u4)( (u4)((p)[0])<<24 & 0xFF000000 | (u4)((p)[1])<<16 & 0x00FF0000 | (u4)((p)[2])<<8 & 0x0000FF00| (u4)((p)[3]) & 0x000000FF)
#define getu2(p) (u2)((p)[0]<< 8 & 0x0000FF00 |(p)[1] & 0x000000FF)
using namespace std;

class Object;

typedef enum {
    ACC_PUBLIC = 0x0001,
    ACC_FINAL = 0x0010,
    ACC_SUPER = 0x0020,
    ACC_NATIVE = 0x0100,
    ACC_INTERFACE = 0x0200,
    ACC_ABSTRACT = 0x0400,
    ACC_SYNTHETIC = 0x1000,
    ACC_ANNOTATION = 0x2000,
    ACC_ENUM = 0x4000
} AccessFlags;

typedef struct {
    uint16_t name_idx;
    uint32_t length;
    char *info;
} Attribute;

/* A wrapper for FILE structs that also holds the file name. */
typedef struct {
    const char *file_name;
    FILE *file;
} ClassFile;

typedef struct {
    uint32_t high;
    uint32_t low;
} Double;

/* Wraps references to an item in the constant pool */
typedef struct {
    uint16_t class_idx;
    uint16_t name_idx;
} Ref;

typedef struct {
    
    uint16_t length;
    char *value;
  
} String;

typedef struct {
    uint32_t high;
    uint32_t low;
} Long;

typedef struct Variable {
    void operator=(const Variable &var){
        
        this->type = var.type;
        switch(type){
            case 1: this->value.charVar = var.value.charVar;break;
            case 2: this->value.shortVar = var.value.shortVar;break;
            case 3: this->value.intVar = var.value.intVar;break;
            case 4: this->value.floatVar = var.value.floatVar;break;
            case 5: this->value.longVar = var.value.longVar;break;
            case 6: this->value.doubleVar = var.value.doubleVar;break;
            case 7: this->value.objectVar = var.value.objectVar;break;
            case 8: this->value.stringVar.length = var.value.stringVar.length;
                    this->value.stringVar.value = var.value.stringVar.value;
            break;
            case 9: this->value.refVar = var.value.refVar;break;
            default: this->value.intVar = 0;
        }
    } 
    int returnInt(){        
        switch(type){
            case 1: return this->value.charVar;break;
            case 2: return this->value.shortVar;break;
            case 3: return this->value.intVar;break;            
            default: this->value.intVar = 0;break;
        }
        return 0;
    }
    void printVar() {
        switch(type){
            case 1: cout<<"char value: "<<this->value.charVar<<endl; break;
            case 2: cout<<"short value: "<<this->value.shortVar<<endl; break;
            case 3: cout<<"int value: "<<this->value.intVar<<endl; break;
            case 4: cout<<"float value: "<<this->value.floatVar<<endl;break;
         //   case 5: cout<<"long value: "<<this->value.longVar<<endl;break;
        //    case 6: cout<<"double value: "<<this->value.doubleVar<<endl;break;
            case 7: cout<<"object value: " <<this->value.objectVar<<endl;break;
            case 8: cout<<"string value: "<<this->value.stringVar.value<<endl;break;
          //  case 9: cout<<"ref value: " <<this->value.refVar<<endl;break;
            default: cout<<"deafult"<<this->value.intVar<<endl;
        }
    }
    short type;

    union {
        char charVar;
        int16_t shortVar;
        int32_t intVar;
        float floatVar;
        Long longVar;
        Double doubleVar;
        Object * objectVar;
        String stringVar;
        Ref refVar;
    } value;
};

typedef enum variableTypes {
    CHARVAR = 1,
    SHORTVAR = 2,
    INTEGERVAR = 3,
    FLOATVAR = 4,
    LONGVAR = 5,
    DOUBLEVAR = 6,
    OBJECTVAR = 7,
    STRINGVAR = 8,
    REFVAR = 9
}variableTypes;

typedef struct {
    uint16_t flags;
    uint16_t name_idx;
    uint16_t desc_idx;
    uint16_t attrs_count;
    Attribute *attrs;
    Variable value;
} Field;

typedef struct {
    uint16_t flags;
    uint16_t name_idx;
    uint16_t desc_idx;
    uint16_t attrs_count;
    Attribute *attrs;
} Method;

typedef struct {
    uint8_t tag; // the tag byte

    union {
        String string;
        float flt;
        Double dbl;
        Long lng;
        int32_t integer;
        Ref ref; /* A method, field or interface reference */
    } value;
} Item;

typedef struct {
    Object * objectHeapPtr;
    char type;

} ObjectPtr;

/* The .javaClass structure */
typedef struct {
    const char *file_name;
    uint16_t minor_version;
    uint16_t major_version;
    uint16_t const_pool_count;
    uint32_t pool_size_bytes;
    Item *items;
    uint16_t flags;
    uint16_t this_class;
    uint16_t super_class;
    uint16_t interfaces_count;
    Ref *interfaces;
    uint16_t fields_count;
    Field *fields;
    uint16_t methods_count;
    Method *methods;
    uint16_t attributes_count;
    Attribute *attributes;
    
    Variable * staticFields;
    int staticFieldsCnt;
    
    String lastMethodName;
    String lastMethodDesc;
    int lastMethodIdx;
    
    String lastFieldName;
    String lastFieldDesc;
    int lastFieldIdx;
} JavaClass;

typedef enum {
    STRING_UTF8 = 1, /* occupies 2+x bytes */
    INTEGER = 3, /* 32bit two's-compliment big endian int */
    FLOAT = 4, /* 32-bit single precision */
    LONG = 5, /* Long: a signed 64-bit two's complement number in big-endian format (takes two slots in the constant pool table) */
    DOUBLE = 6, /* Double: a 64-bit double-precision IEEE 754 floating-point number (takes two slots in the constant pool table) */
    CLASS = 7, /* Class reference: an index within the constant pool to a UTF-8 string containing the fully qualified javaClass name (in internal format) */
    STRING = 8, /* String reference: an index within the constant pool to a UTF-8 string */
    FIELD = 9, /* Field reference: two indexes within the constant pool, the first pointing to a Class reference, the second to a Name and Type descriptor. */
    METHOD = 10, /* Method reference: two indexes within the constant pool, the first pointing to a Class reference, the second to a Name and Type descriptor. */
    INTERFACE_METHOD = 11, /* Interface method reference: two indexes within the constant pool, the first pointing to a Class reference, the second to a Name and Type descriptor. */
    NAME = 12, /* Name and type descriptor: 2 indexes to UTF-8 strings, the first representing a name and the second a specially encoded type descriptor. */
    METHOD_HANDLE = 15,
    METHOD_TYPE = 16,
    INVOKE_DYNAMIC = 18
} CPool_t;
static char *CPool_strings[] = {
    "Undefined", // 0
    "String_UTF8",
    "Undefined", // 2
    "Integer",
    "Float",
    "Long",
    "Double",
    "Class",
    "String",
    "Field",
    "Method",
    "InterfaceMethod",
    "Name",
    "Undefined", // 13
    "Undefined", // 14
    "MethodHandle",
    "MethodType",
    "InvokeDynamic"
};

enum RANGES {
    /* The smallest permitted value for a tag byte */
    MIN_CPOOL_TAG = 1,
    /* The largest permitted value for a tag byte */
    MAX_CPOOL_TAG = 18
};
/* Delegate to read_class(ClassFile) */
JavaClass *read_class_from_file_name(char *f);
/* Parse the given javaClass file into a Class struct. */
JavaClass *read_class(const ClassFile class_file);
/* Parse the attribute properties from file into attr. Assumes class_file.file is at offset relative to reading an attribute struct.
 * See section 4.7 of the JVM spec. */
void parse_attribute(ClassFile class_file, Attribute *attr);
/* Parse the constant pool into javaClass from class_file. ClassFile.file MUST be at the correct seek point i.e. byte offset 11.
 * The number of bytes read is returned. A return value of 0 signifies an invalid constant pool and javaClass may have been changed.
 * See section 4.4 of the JVM spec.
 */
void parse_const_pool(JavaClass *javaClass, const uint16_t const_pool_count, const ClassFile class_file);
/* Parse the initial section of the given class_file up to and including the constant_pool_size section */
void parse_header(ClassFile class_file, JavaClass *javaClass);
/* Return true if class_file's first four bytes match 0xcafebabe. */
bool is_class(FILE *class_file);
/* Return the item pointed to by cp_idx, the index of an item in the constant pool */
Item *get_item(const JavaClass *javaClass, const uint16_t cp_idx);
/* Resolve a Class's name by following javaClass->items[index].ref.class_idx */
Item *get_class_string(const JavaClass *javaClass, const uint16_t index);
/* Convert the high and low bits of dbl to a double type */
double to_double(const Double dbl);
/* Convert the high and low bits of lng to a long type */
long to_long(const Long lng);
/* Convert the 2-byte field type to a friendly string e.g. "J" to "long" */
char *field2str(const char fld_type);

/* Convert tag byte to its string name/label */
static inline char *tag2str(uint8_t tag) {
    return CPool_strings[tag];
}
/* Write the name and javaClass stats/contents to the given stream. */
void print_class(FILE *stream, const JavaClass *javaClass);

int getMethodIdxFromClass(JavaClass* javaClass, string name, string desc);
int compareString(String s, string s2);
int getMethodIdxFromClass(JavaClass* javaClass, String name, String desc);
int compareString(String s, String s2);
int getFieldIdxFromClass(JavaClass* javaClass, String name, String desc);
#endif //CLASS_H__