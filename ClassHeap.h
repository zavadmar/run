/* 
 * File:   ClassHeap.h
 * Author: martin
 *
 * Created on 12. leden 2017, 13:51
 */

#ifndef CLASSHEAP_H
#define	CLASSHEAP_H

#include "JavaClass.h"
#include "Execution.h"
class Execution;
class ClassHeap {
public:
    ClassHeap();
    ClassHeap(const ClassHeap& orig);
    virtual ~ClassHeap();
    
    bool addClass(JavaClass *javaClass);
    JavaClass* getClass(string className);
    JavaClass* getClass(String className);
    JavaClass* getClass(int idx);
    void startClinit(Execution * ex, JavaClass * javaClass);
    void clinitAll(Execution * ex);
    int getClassCnt(){return classCnt;}
    JavaClass * checkCache(String className);
    JavaClass * checkCache(string className);
    
private:
    JavaClass ** heap;
    int heapSize;
    int classCnt;
    
    //inline cache
    String lastClassName;
    JavaClass *lastClass;
    

};

#endif	/* CLASSHEAP_H */

