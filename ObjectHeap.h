/* 
 * File:   ObjectHeap.h
 * Author: martin
 *
 * Created on 12. leden 2017, 14:45
 */

#ifndef OBJECTHEAP_H
#define	OBJECTHEAP_H

#include "Object.h"
#include "Frame.h"
#include "StringObject.h"
#include "ClassHeap.h"
class Frame;
class ClassHeap;

class ObjectHeap {
public:
    ObjectHeap(Frame ** frameStack, ClassHeap *classHeap);
    ObjectHeap(const ObjectHeap& orig);
    virtual ~ObjectHeap();
    
    Object * getObject(int idx);
    
    Object * createObject(JavaClass *javaClass, int frameCnt);
    Object * createArray(unsigned char type, int count,int frameCnt);
    Object * createAArray(JavaClass * javaClass, int count,int frameCnt);
    Object * createString(int frameCnt);
    void goToOldHouse(Object * oldObject);
  
   
private:
    void startGarbageCollection(int frameCnt);
    
    Frame ** frameStack;
   
    ClassHeap * classHeap;
    
    Object ** youngObjectHeap;
    int youngHeapSize;
    int youngObjectCnt;
    
    Object ** oldObjectHeap;
    int oldHeapSize;
    int oldObjectCnt;
};

#endif	/* OBJECTHEAP_H */

