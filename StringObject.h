/* 
 * File:   StringObject.h
 * Author: martin
 *
 * Created on 23. leden 2017, 11:57
 */

#ifndef STRINGOBJECT_H
#define	STRINGOBJECT_H
#include "Object.h"
class StringObject : public Object {
public:
    StringObject();
    StringObject(const StringObject& orig);
    virtual ~StringObject();
    void  setField(int idx, Variable var);
    Variable getField(int idx);
    void markChildren() {return;}
private:

};

#endif	/* STRINGOBJECT_H */

