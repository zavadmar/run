/* 
 * File:   ObjectHeap.cpp
 * Author: martin
 * 
 * Created on 12. leden 2017, 14:45
 */

#include <stddef.h>

#include "ObjectHeap.h"
#include "Object.h"
#include "Array.h"
#include "ClassHeap.h"
#include <iostream>
using namespace std;

ObjectHeap::ObjectHeap(Frame ** frameStack, ClassHeap *classHeap) {
    this->youngObjectCnt = 0;
    this->youngHeapSize = 7;
    this->youngObjectHeap = new Object*[this->youngHeapSize];
    this->oldObjectCnt = 0;
    this->oldHeapSize = 5;
    this->oldObjectHeap = new Object*[this->oldHeapSize];
    this->frameStack = frameStack;
    this->classHeap = classHeap;
}

ObjectHeap::ObjectHeap(const ObjectHeap& orig) {
}

ObjectHeap::~ObjectHeap() {
    for(int i = 0; i<youngObjectCnt; i++){
        delete youngObjectHeap[i];
    }
    delete[] youngObjectHeap;
    
    for(int i = 0; i<oldObjectCnt; i++){
        delete oldObjectHeap[i];
    }
    delete[] oldObjectHeap;
    
}

Object * ObjectHeap::createObject(JavaClass *javaClass,int frameCnt) {
    if (this->youngObjectCnt >= this->youngHeapSize) startGarbageCollection(frameCnt); 
    this->youngObjectHeap[this->youngObjectCnt] = new Object(javaClass);
    this->youngObjectCnt++;
    return youngObjectHeap[this->youngObjectCnt-1];
}

Object * ObjectHeap::createString(int frameCnt) {
    if (this->youngObjectCnt >= this->youngHeapSize) startGarbageCollection(frameCnt); 
    this->youngObjectHeap[this->youngObjectCnt] = new StringObject();
    this->youngObjectCnt++;
    return youngObjectHeap[this->youngObjectCnt-1];
}

void ObjectHeap::startGarbageCollection(int frameCnt){
    cout<< "starting young GC"<<endl;
    
    // MARK
    for (int i = 0 ; i< frameCnt; i++){
        //cout<<frameCnt;
        for (int j = 0; j< frameStack[i]->getMaxLocals(); j++){
            if (frameStack[i]->getLocal(j).type == OBJECTVAR){
                frameStack[i]->getLocal(j).value.objectVar->mark();                
            }            
        }
        for (int j = 0; j< frameStack[i]->getSp(); j++){
            if (frameStack[i]->getStack(j).type == OBJECTVAR){
                frameStack[i]->getStack(j).value.objectVar->mark();                
            }            
        }
    }
    Variable v;
    for(int i = 0; i< classHeap->getClassCnt(); i++){
        for (int j = 0; j<classHeap->getClass(i)->staticFieldsCnt; j++){
                       
            v = classHeap->getClass(i)->staticFields[j];
            if (v.type == OBJECTVAR) {
                classHeap->getClass(i)->staticFields[j].value.objectVar->mark();
            }
        }
    }
    //SWEEP young
    int markedObjCnt = 0;
   
    for (int i = 0; i < youngObjectCnt; i++){
        if (youngObjectHeap[i]->isReachable()) {            
            
            youngObjectHeap[i]->incAge();
            if (youngObjectHeap[i]->isOld()) { 
                goToOldHouse(youngObjectHeap[i]);
              
                youngObjectHeap[i] = NULL;
            }
            else {
              
                youngObjectHeap[markedObjCnt] = youngObjectHeap[i];                
                markedObjCnt++;
            }
            
            continue;
        }
        else{
            delete youngObjectHeap[i];
            youngObjectHeap[i] = NULL;
          
        }
        
    }
    if (markedObjCnt >= youngObjectCnt) {    
        goToOldHouse(youngObjectHeap[youngObjectCnt-1]);
        youngObjectHeap[youngObjectCnt-1] = NULL;
        markedObjCnt--;
    }
    youngObjectCnt = markedObjCnt;
    
    //UNMARK ALL
    for (int i = 0; i< youngObjectCnt; i++){
        youngObjectHeap[i]->unmark();
    }
    for (int i = 0; i< oldObjectCnt; i++){
        oldObjectHeap[i]->unmark();
    }
    
}

void ObjectHeap::goToOldHouse(Object * oldObject){
    cout<< "starting old GC"<<endl;
    this->oldObjectHeap[oldObjectCnt] = oldObject;
    this->oldObjectCnt++;
    
    if (this->oldObjectCnt >= this-> oldHeapSize){
        //SWEEP old
        cout<<"Sweep oldObjectHeap"<<endl;
        int markedObjCnt = 0;
        for(int i = 0; i< this->oldObjectCnt; i++){
            if (oldObjectHeap[i]->isReachable()){
          //      oldObjectHeap[i]->unmark();
                oldObjectHeap[markedObjCnt] = oldObjectHeap[i];
                markedObjCnt++;
                continue;
            }
            else {
                delete oldObjectHeap[i];
                oldObjectHeap[i] = NULL;
            }            
        }
        if (markedObjCnt >= oldObjectCnt) {
            cout<<"Expanding oldObjectHeap"<<endl;
            oldHeapSize *= 2;
            Object ** newOldHeap = new Object*[oldHeapSize];
            for (int i = 0; i< oldObjectCnt; i++){
                newOldHeap[i] = oldObjectHeap[i];
            }
            delete[] oldObjectHeap;
            oldObjectHeap = newOldHeap;
        }
        oldObjectCnt = markedObjCnt;
    }
    
    
}

Object * ObjectHeap::createArray(unsigned char type, int count,int frameCnt){
    if (this->youngObjectCnt >= this->youngHeapSize) startGarbageCollection(frameCnt); 
    this->youngObjectHeap[this->youngObjectCnt] = new Array(type,count,NULL);
    this->youngObjectCnt++;
    return youngObjectHeap[this->youngObjectCnt-1];
}



Object * ObjectHeap::createAArray(JavaClass * javaClass, int count,int frameCnt){
    if (this->youngObjectCnt >= this->youngHeapSize) startGarbageCollection(frameCnt); 
    this->youngObjectHeap[this->youngObjectCnt] = new Array(0,count,javaClass);
    this->youngObjectCnt++;
    return youngObjectHeap[this->youngObjectCnt-1];
}

